function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Button } from "@blueprintjs/core";
import gql from "graphql-tag";
import { withApollo } from "react-apollo";
import compose from "recompose/compose";
import { __ } from "react-pe-utilities";

class WPMediaUploader extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onUpload", evt => {
      const data = `{post_title:"${this.props.name ? this.props.name : ""}", source:"${this.props.url}"}`; //console.log(data)

      const mutation = gql`mutation changeMedia{ changeMedia(input:${data}){id,url} }`;
      this.props.client.mutate({
        mutation
      }).then(result => {
        //console.log(result.data.changeMedia)	
        if (this.props.onUpload) {
          this.props.onUpload(result.data.changeMedia);
        }
      });
    });

    this.state = {};
  }

  render() {
    const {
      url
    } = this.props; //console.log( this.props )

    const enabled = typeof url == "string" && url.indexOf("data:image/") == 0;
    return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Button, {
      onClick: this.onUpload,
      disabled: !enabled,
      className: "m-2"
    }, __("Upload to remote library")));
  }

}

export default compose(withApollo)(WPMediaUploader);