import React, { Component } from "react";
import { getByRoute } from "react-pe-layouts";
export default class ContentByRoute extends Component {
  render() {
    const cont = getByRoute(this.props.routing);
    return /*#__PURE__*/React.createElement("div", {
      className: ""
    }, cont);
  }

}