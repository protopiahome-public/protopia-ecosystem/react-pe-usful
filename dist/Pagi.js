function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import ReactPaginate from "react-paginate";
import { NumericInput } from "@blueprintjs/core";
import { initArea } from "react-pe-utilities";
export default class Pagi extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onChoose", evt => {
      const i = parseInt(evt.currentTarget.getAttribute("data-i"));
      this.setState({
        current: i
      });
      this.props.onChoose(i);
    });

    _defineProperty(this, "onChooseValue", valueAsNumber => {
      const i = valueAsNumber - 1;
      this.setState({
        current: i
      });
      this.props.onChoose(i);
    });

    this.state = {
      all: props.all,
      current: props.current
    };
  }

  componentWillReceiveProps(nextProps) {
    //console.log( nextProps );
    this.setState({
      all: nextProps.all,
      current: nextProps.current
    });
  }

  render() {
    const {
      all,
      current
    } = this.state;
    const {
      marginPagesDisplayed,
      pageRangeDisplayed
    } = this.props;
    return typeof current !== "undefined" ? /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(ReactPaginate, {
      previousLabel: "",
      nextLabel: "",
      breakLabel: "...",
      breakClassName: "break-me",
      pageCount: all + 1,
      marginPagesDisplayed: marginPagesDisplayed ? marginPagesDisplayed : 2,
      pageRangeDisplayed: pageRangeDisplayed ? pageRangeDisplayed : 2,
      onPageChange: data => this.props.onChoose(data.selected),
      containerClassName: "pagination pagination-sm",
      pageClassName: "page-item",
      pageLinkClassName: "page-link",
      previousClassName: "page-item",
      previousLinkClassName: "page-link",
      nextClassName: " page-item",
      nextLinkClassName: "page-link",
      breakLinkClassName: "page-link",
      activeClassName: "active",
      forcePage: current
    }, "adadf"), /*#__PURE__*/React.createElement(NumericInput, {
      large: false,
      min: 1,
      max: this.props.all + 1,
      value: current + 1,
      className: "input dark ml-auto",
      onValueChange: this.onChooseValue
    }), initArea("pagination", { ...this.props,
      ...this.state
    })) : null;
  }

  render2() {
    const {
      all,
      current
    } = this.state;
    const btns = [];

    for (let i = 0; i < all; i++) {
      const ii = i + 1;
      const cls = i == current ? " page-item active " : " page-item "; // console.log(cls);

      btns.push( /*#__PURE__*/React.createElement("li", {
        className: cls,
        key: i
      }, /*#__PURE__*/React.createElement("div", {
        className: "page-link",
        onClick: this.onChoose,
        "data-i": i
      }, ii, " ")));
    }

    return /*#__PURE__*/React.createElement("ul", {
      className: "pagination pagination-sm"
    }, btns);
  }

} // https://github.com/AdeleD/react-paginate/blob/master/demo/js/demo.js