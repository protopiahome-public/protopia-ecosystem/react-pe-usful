function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { Component } from "react";
import TextEditorSince from "./TextEditorSince";
export default class TextEditor extends Component {
  render() {
    return /*#__PURE__*/React.createElement(TextEditorSince, _extends({}, this.props, {
      onChange: this.props.onChange
    }));
  }

}