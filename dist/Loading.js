import React, { Component, useState } from "react";
export default class Loading extends Component {
  render() {
    return /*#__PURE__*/React.createElement("div", {
      className: "loader-cont"
    }, /*#__PURE__*/React.createElement("div", {
      className: "loader fa-spin " + this.props.classNames
    }));
  }

}
export const LoaderLine = props => {
  const [fill] = useState(props.fill || "#444");
  return /*#__PURE__*/React.createElement("div", {
    style: {
      width: "100%",
      height: 70,
      display: "flex",
      justifyContent: "center"
    }
  }, /*#__PURE__*/React.createElement("svg", {
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg",
    xmlnsXlink: "http://www.w3.org/1999/xlink",
    x: "0px",
    y: "0px",
    viewBox: "0 0 100 100",
    enableBackground: "new 0 0 0 0",
    xmlSpace: "preserve"
  }, /*#__PURE__*/React.createElement("circle", {
    fill: fill,
    stroke: "none",
    cx: "6",
    cy: "50",
    r: "6"
  }, /*#__PURE__*/React.createElement("animateTransform", {
    attributeName: "transform",
    dur: "1s",
    type: "translate",
    values: "0 15 ; 0 -15; 0 15",
    repeatCount: "indefinite",
    begin: "0.1"
  })), /*#__PURE__*/React.createElement("circle", {
    fill: fill,
    stroke: "none",
    cx: "30",
    cy: "50",
    r: "6"
  }, /*#__PURE__*/React.createElement("animateTransform", {
    attributeName: "transform",
    dur: "1s",
    type: "translate",
    values: "0 10 ; 0 -10; 0 10",
    repeatCount: "indefinite",
    begin: "0.2"
  })), /*#__PURE__*/React.createElement("circle", {
    fill: fill,
    stroke: "none",
    cx: "54",
    cy: "50",
    r: "6"
  }, /*#__PURE__*/React.createElement("animateTransform", {
    attributeName: "transform",
    dur: "1s",
    type: "translate",
    values: "0 5 ; 0 -5; 0 5",
    repeatCount: "indefinite",
    begin: "0.3"
  }))));
};