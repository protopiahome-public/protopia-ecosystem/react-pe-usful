import React, { Component } from "react"
import { Button,  Classes, Dialog, Intent, Position, Toaster } from "@blueprintjs/core" 
import { __ } from "react-pe-utilities"  

export const AppToaster = Toaster.create({
  position: Position.BOTTOM_RIGHT,
  maxToasts: 4,
})
 

export class DeleteButton extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isRemoveDialog: false,
    }
  }

	onRemoveDialog = () => {
	  this.setState({ isRemoveDialog: !this.state.isRemoveDialog })
	}

	onRemove = () => {
	  this.setState({ isRemoveDialog: !this.state.isRemoveDialog })
	  this.props.onRelease()
	}

	render() {
	  return this.props.isDisabled ? <span /> : (
      <>
        <div className="btn bg-danger tx-wt" onClick={this.onRemoveDialog}>
          <i className="fas fa-trash-alt mr-2" />
          {" "}
          {__("Delete")}
        </div>
        <Dialog
          icon="trash"
          isOpen={this.state.isRemoveDialog}
          onClose={this.onRemoveDialog}
          title={`${__("Do you realy want delete ") + this.props.post_title}?`}
        >
          <div className="pt-dialog-footer">
            <div className={Classes.DIALOG_BODY} />
            <div className={Classes.DIALOG_FOOTER}>
              <div className={Classes.DIALOG_FOOTER_ACTIONS}>

                <Button
                  onClick={this.onRemoveDialog}
                  text={__("oh, No! Sorry")}
                />
                <Button
                  intent={Intent.PRIMARY}
                  onClick={this.onRemove}
                  text={__("Yes, I want finaly delete this")}
                />
              </div>
            </div>
          </div>
        </Dialog>
      </>
	  )
	}
}
