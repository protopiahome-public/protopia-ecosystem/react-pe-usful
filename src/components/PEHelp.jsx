import { Dialog, Position } from "@blueprintjs/core"
import React from "react"
import { useState } from "react"
import { __ } from "react-pe-utilities"
import { Loading } from "react-pe-useful" 
import { gitlab_private_token } from "settings/config" 
import { useEffect } from "react"
import ReactMarkdown from 'react-markdown'

const PEHelp = props => { 
    const [loading, changeLoading] = useState(true)
    const [helps, onHelps] = useState([])

    const tag_url = props.url 
        ?
        props.url
        :
        "Создание-новой-страницы"
    const url = 'https://gitlab.com/api/v4/projects/22486508/wikis?with_content=1'
    const wiki_url = "https://gitlab.com/protopiahome/inner-projects/oraculi/unstable/oraculi-react/-/wikis/"
    const loadList = () => {
        fetch(
            url,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Security-Policy': 'policy',
                    'PRIVATE-TOKEN': gitlab_private_token()
                }
            }
        )
            .then(response => response.json())
            .then(res => {
                //console.log(res)
                if (Array.isArray(res)) {
                    const _helps = res.map((help, i) => {
                        //console.log(help)
                        const re = new RegExp(`<a href="${wiki_url}`, `g`);
                        const newString = `<a class="landing-help-inner-link href="${wiki_url}" `
                        const cont = help.content
                            .replaceAll(`](uploads/`, `](${wiki_url}uploads/`)
                            .replaceAll(re, newString)
                        return tag_url === help.slug
                            ?
                            <div className="landing-help-article" slug={help.slug} key={i}>
                                <div className="landing-help-a-title">
                                    {help.title}
                                </div>
                                <div className="landing-help-a-content">
                                    <ReactMarkdown
                                        children={cont}
                                        linkTarget={"_bulk"}
                                        transformLinkUri={(href, children, title) => {
                                            return href
                                        }}
                                    />
                                </div>
                            </div>
                            :
                            null
                    })
                    onHelps(_helps)
                }
                changeLoading(false)
            },
            error => console.error("Error load help"))
    }
    useEffect(() => {
        loadList()
    }, [])
    useEffect(() => {
        loadList()
    }, [props.url])
    return <Dialog
        isOpen={props.isOpen}
        onClose={props.onClose}
        position={Position.RIGHT}
        size={800}
        className="pe-outer-container"
        usePortal
        title={__("Help")}
    >
        {
            loading
                ?
                <div className="w-100 h-100 d-flex flex-column align-items-center ">
                    <Loading />
                </div>
                :
                <div className="w-100 h-100 d-flex flex-column align-items-center overflow-y-auto">
                    {helps}
                </div>
        }

    </Dialog>
}

export default PEHelp


