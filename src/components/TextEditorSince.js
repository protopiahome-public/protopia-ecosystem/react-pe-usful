import React, { Component } from "react"
import {CKEditor} from "@ckeditor/ckeditor5-react"
import ClassicEditor from "@ckeditor/ckeditor5-build-classic" 

class TextEditorSince extends Component {
	state = {
		text: this.props.text || "",
	}
	

	render() {
		const { text } = this.state
		return (
			<div className="w-100">
				<CKEditor
					editor={ClassicEditor}
					config={{ 
						height: 100,
						
						// language: 'ru',
						toolbar: {
							items: [ 
								'heading', '|',
								'fontFamily', 'fontSize', 'fontColor', 'fontBackgroundColor', '|',
								// 'alignment', '|', 
								'bold', 'italic', 'strikethrough', 'underline', 'subscript', 'superscript', 'code', '|',
								'link', '|',
								'outdent', 'indent', '|',
								'bulletedList', 'numberedList', 'todoList', '|',
								//'code', 'codeBlock', '|',
								'insertTable', '|',
								'blockQuote', '|',
								'undo', 'redo', '|'
							],
							shouldNotGroupWhenFull: true
						}, 						
					}}
					data={text}
					onChange={this.onChange}
					onBlur={this.onBlur}
					onFocus={this.onFocus}
					onReady={ this.onReady }
					rows={10}
				/>
			</div>
		)
	}
	onReady = editor => {
		// You can store the "editor" and use when it is needed.
		console.log( 'Editor is ready to use!', editor );
	}

	onInit = () => {

	}

	onChange = (event, editor) => {
		const data = editor.getData()
		this.props.onChange(data)
		console.log({ event, editor, data })
	}

	onBlur = (event, editor) => {
		console.log("Blur.", editor)
	}

	onFocus = (event, editor) => {
		console.log("Focus.", editor)
	}
}

export default TextEditorSince
/* https://ckeditor.com/docs/ckeditor5/latest/builds/guides/integration/frameworks/react.html */
